FROM python:3.8-slim-buster

WORKDIR /app

ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1
ENV DEBUG 0

RUN python -m pip install --upgrade pip
COPY ./requirements.txt .
RUN pip install -r /app/requirements.txt

COPY . .

RUN python /app/manage.py collectstatic --noinput

RUN adduser myuser
USER myuser

EXPOSE 5000

CMD gunicorn --chdir /app mysite.wsgi:application --bind 0.0.0.0:5000