from django.test import TestCase
import os

# Create your tests here.
class ExampleTestCase(TestCase):    
    def test_requirement_file(self):
        """Is requirement.txt exists"""
        dependencies_file = "./requirements.txt"
        self.assertTrue(os.path.exists(dependencies_file))